FROM python:3.7-alpine
MAINTAINER sentaipadas
ENV PYTHONUNBUFFERED 1

RUN \
	apk add --no-cache postgresql-libs && \
	apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev


COPY ./requirements.txt /requirements.txt 
RUN pip install -r /requirements.txt

RUN  apk --purge del .build-deps

RUN mkdir /app
WORKDIR /app
COPY ./app /app

RUN adduser -D user
USER user