## Setup 
1. Clone the project:
```
git clone https://gitlab.com/sentaipadas/tictactoe-backend.git
```

2. To build the project run:
```
docker-compose build
```
3. To setup Postgres run:
```
docker-compose run app sh -c "python manage.py makemigrations"
docker-compose run app sh -c "python manage.py migrate"
```

4. (Optional) to inspect data with admin interface run:
```
docker-compose run app sh -c "python manage.py createsuperuser --email admin@example.com --username admin"
```

## Launch 

To start the live server run:
```
docker-compose up
```

API is accessible through `http://localhost:3000` and routes described below.

## Design

### interpretation of the game

TicTacToe can be viewed as a 9 turn game on 9 squares. In this API the squares are bit encoded by columns like so:

|   |   |   |
| - |-  | - |
|1  | 8 |64 |
|2  | 16|128|
|4  | 32|256|

Game is tracked by turns, which range from 0 to 9 stored in variable `turn`.  
Players activity is tracked for the latest turn as bitmasks in variables `player1_history`, `player2_history` for player 1 (X) and player 2 (O) resp.  
So for a example a valid game at `turn=6` could be:

|   |   |   |
| - |-  | - |
|X  | O | X |
|O  |   | O |
|   | X |   |

For which corresponding histories would be: `player1_history=97`, `player2_history=138`

#### playing

A valid `move` is single-bit mask on an empty square which is done by player 1 if currently `turn` is even and by player 2 otherwise. After a move is done `turn` is incremented by 1 and corresponding playing history is updated with bitwise `or`.

#### end of the game

There are 6 possible winning scenarios: one player get 3 in a row, column or diagonal. All 6 cases be easily encoded using above interpretation and checked by using bitwise `and` against required player history after each turn.  
If after 9 turns no victory is attained the game ends in a `draw`.
### Models

There are 3 models used in this project: 
1. `Game` - singleton model for tracking currently active game. The model has columns for tracking variables described in the previous section:
	1. `winner` - state of the game, 0 for `unfinished`, 1-2 for winning of `player 1`, `player 2` resp, 3 for `draw`.
	2. `turn` 
	3. `player1_history`, `player2_history`
2. `Log` - log of each request made to the API which keeps track of all the attributes of the currently active game, as well as:
	1. `action` - one of `GAME_LOAD`, `GAME_RESET`, `GAME_MOVE` for loading currently active game, resetting the game, mkaing a move in the currently active game resp.
	2. `move` - bitmask for move made if `action` is `GAME_MOVE` and 0 otherwise

### Routes 

1. `GET api/games/touch` - loads the current state of the game or creates a new one if it's called for the first time, optional query parameter `?new=true` resets the game. Example response:
```
{
    "id": 1,
    "turn": 0,
    "player1_history": 0,
    "player2_history": 0,
    "winner": 0,
    "latest_log": {...}
}
```
2. `POST api/games/move` - make a move with example body:
```
{
	turn: 1,
	move: 16
}
```
If the turn is valid example response is identical to 1. with updated game state, otherwise `400` is returned with a relevant error code.
3. `GET api/logs` - returns the logs, limited to 100 latest by default or optionally to `limit` latest by setting query parameter to e.g. `?limit=50`.
4. `GET /admin` web interface provided by Django, requires step 4. from **Setup** for inspecting and modifying the database.

NOTE: `latest_log` appearing in response of 1. , 2. is the latest `Log` instance created by the request. 

## Testing

There are 6 tests for testing `GET api/games/touch`, `POST api/games/move` for the most common use cases of interacting with the API and one sample test for testing and invalid `move` request. The tests validate that the database is updated correctly and a correct response in returned together with an appropriate log. To run the tests do:
```
docker-compose run app sh -c "python manage.py test"
```