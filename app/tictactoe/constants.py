from functools import reduce

LOG_LIMIT = 100

ACTIONS = [
    (0, 'GAME_LOAD'),
    (1, 'GAME_RESET'),
    (2, 'GAME_MOVE')
]

WINNER_STATES = [
    (0, 'unfinished'),
    (1, 'player 1'),
    (2, 'player 2'),
    (3, 'draw')
]

# game grid encoded in binary bits
# 1 | 8  | 64
# 2 | 16 | 128
# 4 | 32 | 256


def compose_mask(list):
    # turn a list of single bit masks into a single multibit mask
    return reduce((lambda a, b: a | b), list)


WINNING_STATES = [
    *[compose_mask([1 << (x + 3*i) for i in range(3)])
      for x in range(3)],  # winning column masks
    *[compose_mask([1 << (3*x + i) for i in range(3)])
      for x in range(3)],  # winning row masks
    # winning diagonal masks
    *[compose_mask([1, 16, 256]), compose_mask([4, 16, 64])]
]
