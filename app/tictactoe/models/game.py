from django.db import models
from django.core.exceptions import ValidationError

from ..constants import WINNING_STATES, WINNER_STATES

class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if self.__class__.objects.exists() and not self.pk:
            raise ValidationError(
                "there can be only one {}".format(self.__class__.__name__))
        self.__class__.objects.exclude(id=self.pk).delete()

        super(SingletonModel, self).save(*args, **kwargs)

    @classmethod
    def load(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            obj = cls()
            obj.save()
            return obj


class Game(SingletonModel):

    created = models.DateTimeField(auto_now_add=True)

    turn = models.IntegerField(default=0)

    player1_history = models.IntegerField(default=0)
    player2_history = models.IntegerField(default=0)

    winner = models.IntegerField(choices=WINNER_STATES, default=0)

    def make_move(self, move, turn):
        player_name = 2 - (turn % 2)
        player_history_field_name = "player{}_history".format(str(player_name))

        player_history = getattr(self, player_history_field_name) | move

        finished = False
        for STATE in WINNING_STATES:
            if player_history & STATE == STATE:
                # Game is finished and current player has won if player_history up to this point contains any of the winning states
                finished = True
                break
        if finished:
            self.winner = player_name
        elif turn == 9:
            # declare draw if 9 squares have been filled and no on has won yet
            self.winner = 3

        self.turn = turn
        setattr(self, player_history_field_name, player_history)

    def reset(self):
        self.turn = 0
        self.player1_history = 0
        self.player2_history = 0
        self.winner = 0
        self.save()
    
    @classmethod
    def load(cls, new=False):
        resource = super(Game, cls).load()
        if new:
            resource.reset()
        return resource