from django.db import models
from django.core.exceptions import ValidationError

from ..constants import WINNER_STATES, ACTIONS


class Log(models.Model):

    created = models.DateTimeField(auto_now_add=True)

    move = models.IntegerField(default=0)
    turn = models.IntegerField(default=0)

    player1_history = models.IntegerField(default=0)
    player2_history = models.IntegerField(default=0)

    winner = models.IntegerField(choices=WINNER_STATES, default=0)

    action = models.IntegerField(choices=ACTIONS, default=0)

    
    @classmethod
    def create_from_game(cls, action, game, move=0):
        obj = cls(action=action, move=move, turn=game.turn, player1_history=game.player1_history,
                  player2_history=game.player2_history, winner=game.winner)
        obj.save()
        return obj
