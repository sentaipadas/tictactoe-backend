from rest_framework import serializers
from .models import Game, Log


class GameSerializer(serializers.ModelSerializer):
    latest_log = serializers.SerializerMethodField()

    def get_latest_log(self, obj):
        return LogSerializer(obj.latest_log).data

    class Meta:
        model = Game
        fields = ('id', 'turn', 'player1_history',
                  'player2_history', 'winner',  'latest_log')


class LogSerializer(serializers.ModelSerializer):
    action = serializers.CharField(source='get_action_display')
    winner = serializers.CharField(source='get_winner_display')

    class Meta:
        model = Log
        fields = ('created', 'action', 'turn', 'player1_history', 'player2_history',
                  'move', 'winner')