from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from .models import Game
from .serializers import GameSerializer

class GameAPITestCase(APITestCase):
	def setUp(self):
		self.game = Game.load()

class GameTouchAPIViewTestCase(GameAPITestCase):
    
	def test_touch(self):
		url = reverse('game-touch')
		response = self.client.get(url)
		self.assertEqual(200, response.status_code)
		self.assertEqual(self.game.id, response.data['id'])

		self.assertEqual('GAME_LOAD', response.data['latest_log']['action'])


	def test_touch_and_new(self):
		self.game.winner = 3
		self.game.save()
		
		url = reverse('game-touch') + '?new=true'
		response = self.client.get(url)

		self.game.refresh_from_db()
		self.assertEqual(200, response.status_code)
		self.assertEqual(self.game.winner, 0)

		self.assertEqual('GAME_RESET', response.data['latest_log']['action'])


class GameMoveAPIViewTestCase(GameAPITestCase):
	def setUp(self):
		self.game = Game.load()
		self.url = reverse('game-move')

	def test_first_move(self):
		response = self.client.post(self.url, {
			'move': 1 << 4,
			'turn': 1
		}, format='json')

		self.game.refresh_from_db()
		
		self.assertEqual(200, response.status_code)
		self.assertEqual(self.game.id, response.data['id'])
		self.assertEqual(self.game.turn, 1)
		self.assertEqual(self.game.player1_history, 1 << 4)
		self.assertEqual(self.game.player2_history, 0)
		self.assertEqual(self.game.winner, 0)

		self.assertEqual('GAME_MOVE', response.data['latest_log']['action'])

	
	def test_winning_move(self):
		self.game.player1_history = 1 << 0 | 1 << 1
		self.game.player2_history = 1 << 3 | 1 << 4
		self.game.turn = 4
		self.game.save()

		response = self.client.post(self.url, {
			'move': 1 << 2,
			'turn': 5
		}, format='json')

		self.game.refresh_from_db()
		
		self.assertEqual(200, response.status_code)

		self.assertEqual(self.game.turn, 5)
		self.assertEqual(self.game.player1_history, 1 << 0 | 1 << 1 | 1 << 2)
		self.assertEqual(self.game.winner, 1)

	def test_draw_move(self):
		self.game.player1_history = 1 << 0 | 1 << 1 | 1 << 6 | 1 << 7
		self.game.player2_history = 1 << 2 | 1 << 3 | 1 << 4 | 1 << 8 
		self.game.turn = 8
		self.game.save()

		response = self.client.post(self.url, {
			'move': 1 << 5,
			'turn': 9
		}, format='json')

		self.game.refresh_from_db()
		
		self.assertEqual(200, response.status_code)
		self.assertEqual(self.game.winner, 3)

	def test_invalid_request(self):
		response = self.client.post(self.url, {}, format='json')

		self.assertEqual(400, response.status_code)
		
		