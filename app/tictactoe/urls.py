
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import GameTouchView, GameMoveView, LogView

urlpatterns = [
    path('games/touch', GameTouchView.as_view(), name="game-touch"),
    path('games/move', GameMoveView.as_view(), name="game-move"),

    path('logs', LogView.as_view(), name="logs-all"),
]

urlpatterns = format_suffix_patterns(urlpatterns)
