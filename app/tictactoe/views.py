from rest_framework import generics
from rest_framework.response import Response

from rest_framework.exceptions import ValidationError

from .models import Game, Log
from .serializers import GameSerializer, LogSerializer
from .constants import LOG_LIMIT

class GameTouchView(generics.RetrieveAPIView):
    serializer_class = GameSerializer

    def get_object(self):
        new = True if 'new' in self.request.query_params and self.request.query_params[
            'new'] == 'true' else False

        resource = Game.load(new)
        action = 1 if new else 0
        log = Log.create_from_game(action=action, game=resource)
        resource.latest_log = log
        return resource


class GameMoveView(generics.CreateAPIView):
    serializer_class = GameSerializer

    def post(self, request):
        resource = Game.load()

        move, turn = self.sanitize_request(resource)

        resource.make_move(move, turn)
        resource.save()
        
        log = Log.create_from_game(action=2, game=resource, move=move)
        resource.latest_log = log

        return Response(self.serializer_class(resource).data)

    def sanitize_request(self, active_game):
        if not 'move' in self.request.data or not 'turn' in self.request.data:
            raise ValidationError('invalid request')

        move = self.request.data['move']
        turn = self.request.data['turn']

        if not active_game.get_winner_display() == 'unfinished':
            raise ValidationError('game already finished')
        if not active_game.turn + 1 == turn:
            raise ValidationError('invalid turn')
        if not bin(move).count('1') == 1 or move > 1 << 8:
            raise ValidationError('invalid move code')
        if move & active_game.player1_history or move & active_game.player2_history:
            raise ValidationError('move already done')

        return move, turn


class LogView(generics.ListAPIView):
    serializer_class = LogSerializer
    queryset = Log.objects.all().order_by("-created")

    def get_queryset(self):
        params = self.request.query_params
        limit = LOG_LIMIT if 'limit' not in params else int(params['limit'])
        return self.queryset[:limit]
